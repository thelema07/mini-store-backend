import * as express from "express";
import Nodemailer from "nodemailer";

// finish the interface to complete the split of code with the mail service

export default function nodemailerService(
  res: any,
  mailOptions: any,
  mailService: string,
  user: string
) {
  const transporter = Nodemailer.createTransport({
    service: "gmail",
    auth: {
      user,
      pass: "quelquechose"
    }
  });

  transporter.sendMail(mailOptions, async (error, info) => {
    return error
      ? res.status(400).json({ error })
      : res
          .status(200)
          .json({ info, msg: `Email was sent to ${info.envelope.to}` });
  });
}
