const socket = io();

// This function has the initial setup for the course
const chatTrainingMead = () => {
  document.querySelector("#msg-form").addEventListener("submit", e => {
    e.preventDefault();
    const message = document.querySelector("#user-input").value;
    console.log(message);
    socket.emit("chat-msg", message);
  });

  socket.on("confirmation", msg => {
    console.log(msg);
  });

  socket.on("countUpdated", count => {
    console.log("count updated!", count);
  });

  document.querySelector("#increment").addEventListener("click", () => {
    console.log("clicked");
    socket.emit("increment");
  });

  socket.on("user-connected", msg => {
    console.log(msg);
  });

  socket.on("adios", msg => {
    console.log(msg);
  });

  document.querySelector("#send-location").addEventListener("click", () => {
    if (!navigator.geolocation) {
      return alert("Geolocation is not supported");
    }

    navigator.geolocation.getCurrentPosition(position => {
      console.log(position);
      const { latitude, longitude } = position.coords;
      socket.emit("send-location", { latitude, longitude });
    });
  });

  socket.on("share-location", location => {
    console.log(location);
  });
};

chatTrainingMead();

// This is the bootstrap rewrite

const bs4StyledChat = () => {
  document.querySelector("#chat-form").addEventListener("submit", e => {
    e.preventDefault();
    const message = document.querySelector("#exampleFormControlTextarea1")
      .value;
    socket.emit("chat-msg", message);
  });
};

bs4StyledChat();
