const activateChatOperations = (io) => {
  io.on("connection", (socket) => {
    const countUsers = 0;
    console.log("Socket IO!!!");

    let count = 0;

    socket.emit("countUpdated", count);
    socket.on("increment", () => {
      count++;
      // socket.emit("countUpdated", count);
      io.emit("countUpdated", count);
    });
    socket.broadcast.emit("user joined");

    io.emit("user-connected", "User Connected");
    socket.on("chat-msg", (message) => {
      console.log(message);

      io.emit(
        "confirmation",
        message
          .split("")
          .reverse()
          .join(",")
          .toString(),
      );
    });

    socket.on("disconnect", () => {
      io.emit("adios", "a user says adios.");
      console.log("a user says adios");
    });

    socket.on("send-location", (location) => {
      socket.broadcast.emit("share-location", {
        location: `https://google.com/maps?q=${location.latitude},${
          location.longitude
        }`,
        msg: "This is a joke",
      });
    });
  });
};

export { activateChatOperations };
