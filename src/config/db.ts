import mongoose from "mongoose";
import { DataConfig } from "./dataConfig";

export const connectDB = async () => {
  try {
    await mongoose.connect(DataConfig.mongoURI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });
    console.log(`Higher powers spread data from MongoDB`);
  } catch (error) {
    console.log(error.message);
    process.exit(1);
  }
};
