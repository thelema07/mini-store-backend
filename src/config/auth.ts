import jwt from "jsonwebtoken";
import { DataConfig } from "./dataConfig";

//User Authentication Middleware

export default function(req: any, res: any, next: any) {
  const token = req.header("x-auth");

  if (!token) {
    return res
      .status(401)
      .json({ loginError: "Access Denied! Unidentified User" });
  }

  try {
    const decoded = jwt.verify(token, DataConfig.JWTSecret);
    req.user = decoded;
    next();
  } catch (error) {
    res.status(401).json({ loginError: "Invalid Token" });
  }
}
