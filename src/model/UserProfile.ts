import mongoose, { Schema, Document } from "mongoose";

// User profile data model configuration

export interface IUserProfile extends Document {
  user_profile_id: mongoose.Types.ObjectId;
  firstName: String;
  lastName: String;
  profesion: String;
  email: String;
  birthDate: Date;
  ID: Number;
  taxId: Number;
  description: String;
  interests: string[];
}

const UserProfileSchema: Schema = new Schema({
  user_profile_id: {
    type: String,
    required: true,
    unique: true
  },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  profesion: { type: String },
  email: { type: String },
  birthDate: { type: Date, required: true },
  ID: { type: Number },
  taxId: { type: Number, required: true },
  description: { type: String },
  interests: { type: Array }
});

export default mongoose.model<IUserProfile>("UserProfile", UserProfileSchema);
