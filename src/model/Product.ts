import mongoose, { Schema, Document } from "mongoose";

// Product Data model configutarion with its interface

export interface IProduct extends Document {
  ref: number;
  product_name: string;
  product_type: string;
  price: number;
  description: string;
  likes: Array<object>;
  comments: Array<object>;
}

const ProductSchema: Schema = new Schema({
  ref: { type: Number, required: true, unique: true },
  product_name: { type: String, required: true },
  product_type: { type: String, required: true },
  price: { type: Number, required: true },
  description: { type: String, required: true },
  likes: { type: Array },
  comments: { type: Array }
});

export default mongoose.model<IProduct>("Product", ProductSchema);
