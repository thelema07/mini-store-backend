import mongoose from "mongoose";
import User, { IUser } from "../User";
import { DataConfig } from "../../config/dataConfig";

describe("User Model", () => {
  beforeAll(async () => {
    await mongoose.connect(DataConfig.mongoURI, {
      useNewUrlParser: true
    });
  });

  afterAll(async () => {
    mongoose.connection.close();
  });

  it("Should throw validation errors", () => {
    const user = new User();

    expect(user.validate).toThrow();
  });

  it("Should contain a user", async () => {
    const user: IUser = new User({
      name: "Horus Falcon",
      email: "horus777@gmail.com",
      password: "Westworld2019(())"
    });

    const spy = jest.spyOn(user, "save");
    user.save();

    expect(user).toMatchObject({
      name: expect.any(String),
      email: expect.any(String),
      password: expect.any(String)
    });
  });
});
