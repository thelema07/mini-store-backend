import mongoose from "mongoose";
import Product, { IProduct } from "../Product";
import { DataConfig } from "../../config/dataConfig";

describe("Product Model", () => {
  beforeAll(async () => {
    await mongoose.connect(DataConfig.mongoURI, {
      useNewUrlParser: true
    });
  });

  afterAll(async () => {
    mongoose.connection.close();
  });

  it("Should throw validation errors", () => {
    const product = new Product();

    expect(product.validate).toThrow();
  });

  it("Should have a product", async () => {
    const product: IProduct = new Product({
      ref: 777,
      product_name: "Horus The Great",
      product_type: "Magiks Book",
      price: 777
    });

    const spy = jest.spyOn(product, "save");
    product.save();

    expect(spy).toHaveBeenCalled();

    expect(product).toMatchObject({
      ref: expect.any(Number),
      product_name: expect.any(String),
      product_type: expect.any(String),
      price: expect.any(Number)
    });
  });
});
