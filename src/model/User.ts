import mongoose, { Schema, Document } from "mongoose";

// User Data model configutarion with its interface

export interface IUser extends Document {
  name: string;
  email: string;
  password: string;
  avatar: Buffer;
}

const UserSchema: Schema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  avatar: { type: Buffer }
});

export default mongoose.model<IUser>("User", UserSchema);
