export default function ErrorHandler(
  error: any,
  req: any,
  res: any,
  next: Function
) {
  return res.status(400).json({ error: error.message });
}
