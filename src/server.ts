import express from "express";

// App custom routes
import cors from "cors";
import http from "http";
import path from "path";
import socketio from "socket.io";
import { ProductRouter } from "./routes/api/product";
import { UserRouter } from "./routes/api/user";
import { UserProfileRouter } from "./routes/api/userProfile";

import { activateChatOperations } from "./chat/WebSocketOps";

// Setup MongoDB database
import { connectDB } from "./config/db";

connectDB();
// Establish JWT Strategy
export const app = express();
const port = process.env.PORT || 4500;

const server = http.createServer(app);
const io = socketio(server);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Testing socket.io
const directoryPath = path.join(__dirname, "./test-node");
app.use(express.static(directoryPath));

app.use("/api/product", ProductRouter);
app.use("/api/user", UserRouter);
app.use("/api/user-profile", UserProfileRouter);

app.get("/", (req, res) => {
  res.send("This is typescript Server new attempt");
});

activateChatOperations(io);

function startServer() {
  server.listen(port);
  return console.log(`Buddha emits energy at port: ${port}`);
}

startServer();
