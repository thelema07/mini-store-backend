import * as express from "express";
import multer from "multer";
import auth from "../../config/auth";
import ErrorHandler from "../../util/ErrorHandler";
import {
  addUser,
  deleteUserImage,
  getAllUsers,
  getUserImageById,
  loginUser,
  sendEmail,
  uploadUserImage,
} from "../controller/user.controller";

// Multer file limits config an error handling
const upload = multer({
  limits: {
    fileSize: 1000000,
  },
  fileFilter(req: any, file: any, cb: Function) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error("Please upload image files only"));
    }
    cb(undefined, true);
  },
});

const UserRouter = express.Router();

// Routes for product connected to custom controllers

// Add a new user
UserRouter.get("/get-users", getAllUsers);
// Add a new user
UserRouter.post("/add-user", addUser);
// Login a user
UserRouter.post("/login-user", loginUser);
// upload user image
UserRouter.post(
  "/upload/me/avatar",
  auth,
  upload.single("upload"),
  uploadUserImage,
  ErrorHandler,
);
// Delete user image
UserRouter.delete("/upload/me/avatar", auth, deleteUserImage, ErrorHandler);
UserRouter.get("/upload/avatar/:id", auth, getUserImageById, ErrorHandler);
UserRouter.post("/test-tutorials/:id", auth, sendEmail, ErrorHandler);

export { UserRouter };
