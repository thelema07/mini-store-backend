import * as express from "express";

import auth from "../../config/auth";
import ErrorHandler from "../../util/ErrorHandler";
import {
  addInterest,
  addUserProfile,
  deleteProfile,
  getAllProfiles,
  getProfileById,
} from "../controller/userProfile.controller";

const UserProfileRouter = express.Router();

UserProfileRouter.post("/", auth, addUserProfile, ErrorHandler);
UserProfileRouter.get("/all", getAllProfiles, ErrorHandler);
UserProfileRouter.get("/:id", getProfileById, ErrorHandler);
UserProfileRouter.delete("/:id", auth, deleteProfile, ErrorHandler);
UserProfileRouter.post("/add-experience/:id", auth, addInterest, ErrorHandler);

export { UserProfileRouter };
