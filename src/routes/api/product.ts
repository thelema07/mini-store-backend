import * as express from "express";
import {
  addProduct,
  getAllProducts,
  getProductById,
  updateProductById,
  deleteProductById,
  likeProduct,
  addCommentProduct
} from "../controller/product.controller";
import auth from "../../config/auth";
import ErrorHandler from "../../util/ErrorHandler";

const ProductRouter = express.Router();

// Routes for product connected to custom controllers

ProductRouter.post("/add-product", addProduct, ErrorHandler);
ProductRouter.get("/all", getAllProducts, ErrorHandler);
ProductRouter.get("/get-item/:id", auth, getProductById, ErrorHandler);
ProductRouter.put("/update-item/:id", updateProductById, ErrorHandler);
ProductRouter.delete("/delete-item/:id", deleteProductById, ErrorHandler);
ProductRouter.put("/like-product/:id", auth, likeProduct, ErrorHandler);
ProductRouter.put(
  "/comment-product/:id",
  auth,
  addCommentProduct,
  ErrorHandler
);

export { ProductRouter };
