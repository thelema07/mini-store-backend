import { isEmpty } from "./IsEmpty";
import { IProduct } from "../../model/Product";

export const validateEmail = (email: string): boolean => {
  const validate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return validate.test(email) ? true : false;
};

export const validateProductInput = (data: any): any => {
  let errors = [];

  if (!validateEmail(data.email)) {
    errors.push({ type: "Validation", msg: "Email Invalid" });
  }

  if (isEmpty(data.email)) {
    errors.push({ type: "Validation", msg: "Email Required" });
  }

  return errors;
};
