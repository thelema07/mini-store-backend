import { isEmpty } from "../IsEmpty";
import { validateEmail } from "../StoreDataValidator";

test("Validation function must fail if input is not an email", () => {
  const emailInput = validateEmail("este@falla");
  expect(emailInput).toBeFalsy();
});

test("Validation function must succeed if input is an email", () => {
  const emailInput = validateEmail("este@acierta.th");
  expect(emailInput).toBeTruthy();
});

test("is empty function must return false with undefined data", () => {
  expect(isEmpty(undefined)).toBeFalsy();
});

test("is empty function must return true with string or number data", () => {
  expect(isEmpty("Horus")).toBeFalsy();
  expect(isEmpty(1234)).toBeFalsy();
});
