import * as express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { DataConfig } from "../../config/dataConfig";
import sharp from "sharp";
import Nodemailer from "nodemailer";

import {
  validateEmail,
  validateProductInput
} from "../routes_util/StoreDataValidator";

import User from "../../model/User";

export const getAllUsers = async (req: any, res: any) => {
  try {
    const users = await User.find();
    res.status(200).json(users);
  } catch (error) {
    // Basic Error handling
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const addUser = async (req: any, res: any) => {
  let { name, email, password } = req.body;

  let errors = validateProductInput(req.body);
  if (errors.length !== 0) return res.status(400).json({ errors: errors });

  try {
    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ msg: "User already exists" });
    }

    // Generating Salt and hashing password
    bcrypt.genSalt(10, async (err, salt) => {
      bcrypt.hash(password, salt, async (err, hash) => {
        password = hash;
        const newUser = new User({ name, email, password });
        // Saving in DB
        await newUser.save();
        res.status(200).json(newUser);
      });
    });
  } catch (error) {
    // Basic Error handling
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const loginUser = async (req: any, res: any) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res
        .status(400)
        .json({ msg: "User does not exists, please create an account" });
    }

    bcrypt.compare(password, user.password).then(async isMatch => {
      if (isMatch) {
        const payload = { id: user.id, name: user.name };

        await jwt.sign(
          payload,
          DataConfig.JWTSecret,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res.status(400).json({ msg: "Paswords do not match" });
      }
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

// Upload and save in DB user image
export const uploadUserImage = async (req: any, res: any) => {
  try {
    const { id } = req.user;
    const buffer = await sharp(req.file.buffer)
      .png()
      .toBuffer();
    const user = await User.findByIdAndUpdate(
      id,
      { avatar: buffer },
      { new: true }
    );
    await res.status(200).json(user);
  } catch (error) {
    res.send(error);
  }
};

// Delete user image in DB
export const deleteUserImage = async (req: any, res: any) => {
  try {
    const { id } = req.user;
    const user = await User.findById(id);

    if (!user.avatar) {
      res.status(404).json({ msg: "Not User Image Loaded" });
    } else {
      user.avatar = undefined;
      user.save();
      res.status(200).json({ msg: "User Image was deleted" });
    }
  } catch (error) {
    res.send(error);
  }
};

// Get user image by Id
export const getUserImageById = async (req: any, res: any) => {
  try {
    const { id } = req.user;
    const user = await User.findById(id);

    if (!user || !user.avatar) {
      res.status(404).json({ msg: "Not User Image Loaded" });
    }

    res.set("Content-Type", "image/png");
    res.send(user.avatar);
  } catch (error) {
    res.send(error);
  }
};

// This function must change location in a service file

export const nodemailerService = async (
  res: any,
  mailOptions: any,
  mailService: string,
  user: string
) => {
  const transporter = Nodemailer.createTransport({
    service: "gmail",
    auth: {
      user,
      pass: "--------"
    }
  });

  await transporter.sendMail(mailOptions, async (error, info) => {
    return error
      ? res.status(400).json({ error })
      : res
          .status(200)
          .json({ info, msg: `Email was sent to ${info.envelope.to}` });
  });
};

// Send Email Service
export const sendEmail = async (req: any, res: any) => {
  const { html, to, from, subject } = req.body;
  const mailOptions = { from, to, subject, html };
  try {
    await nodemailerService(res, mailOptions, "gmail", to);
  } catch (error) {
    res.status(500).json(error);
  }
};
