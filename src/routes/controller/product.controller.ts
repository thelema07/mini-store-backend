import * as express from "express";

import User from "../../model/User";
import Product from "../../model/Product";

// @Request:  POST -  @Route: /add-product - Public route to post a single product -

export const addProduct = async (req: any, res: any) => {
  const { ref, product_name, product_type, price, description } = req.body;

  try {
    let product = await Product.findOne({ ref });

    if (product) {
      res.status(400).json({ msg: "Product already exists" });
    }

    const newProduct = new Product({
      ref,
      product_name,
      product_type,
      price,
      description
    });
    await newProduct.save();
    res.json({ msg: "Product Saved" });
  } catch (err) {
    res.status(500).json({ msg: "Server Error", payload: err.errors });
  }
};

// @Request:  GET -  @Route: /all - Public route to get all products

export const getAllProducts = async (req: any, res: any) => {
  try {
    const products = await Product.find();
    res.status(200).json(products);
  } catch (err) {
    res.status(500).json({ msg: "Server Error", payload: err.errors });
  }
};

// @Request:  GET -  @Route: /get-one - Public route to get specific product by _id

export const getProductById = async (req: any, res: any) => {
  const { id } = req.params;

  try {
    const product = await Product.findById(id);

    res.json(product);
  } catch (err) {
    if (err.kind === "ObjectId") {
      res.json({ msg: "Product not found" });
    }
  }
};

// @Request:  PUT -  @Route: /get-one - Public route to update specific product by _id

export const updateProductById = async (req: any, res: any) => {
  const { id } = req.params;

  try {
    const product = await Product.findByIdAndUpdate(
      id,
      { $set: req.body },
      { new: true }
    );

    res.status(200).json(product);
  } catch (err) {
    if (err.kind === "ObjectId") {
      res.json({ msg: "Product not found" });
    }

    res.status(500).json({ msg: "Server Error" });
  }
};

// @Request:  DELETE -  @Route: /delete-item - Public route to update specific product by _id

export const deleteProductById = async (req: any, res: any) => {
  const { id } = req.params;

  try {
    const product = await Product.findByIdAndDelete(id);

    if (!product) {
      res.status(404).json({ msg: "Product doesn't exists" });
    }

    res.status(200).json({ msg: "Product Deleted" });
  } catch (err) {
    if (err.kind === "ObjectId") {
      res.json({ msg: "Product not found" });
    }

    res.status(500).json({ msg: "Server Error" });
  }
};

export const likeProduct = async (req: any, res: any) => {
  const user = req.user.id;
  const productId = req.params.id;
  try {
    const product = await Product.findById(productId);

    if (product.likes.filter(like => like === req.user.id).length > 0) {
      return res.status(400).json({ msg: "Post already liked" });
    }

    product.likes.unshift(user);
    product.save();

    res.json(product);
  } catch (err) {
    if (err.kind === "ObjectId") {
      res.json({ msg: "Product not found" });
    }

    res.status(500).json({ msg: "Server Error" });
  }
};

export const addCommentProduct = async (req: any, res: any) => {
  const user = req.user.id;
  const productId = req.params.id;
  const userComment = {
    id: user,
    comment: req.body.comment
  };
  try {
    const product = await Product.findById(productId);

    product.comments.unshift(userComment);
    product.save();

    return res.status(200).json(product);
  } catch (err) {
    if (err.kind === "ObjectId") {
      res.json({ msg: "Product not found" });
    }

    res.status(500).json({ msg: "Server Error" });
  }
};
