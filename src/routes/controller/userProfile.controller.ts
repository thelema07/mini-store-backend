import * as express from "express";

import UserProfile, { IUserProfile } from "../../model/UserProfile";

// @POST - Add or update user profiles - Auth required
export const addUserProfile = async (req: any, res: any) => {
  const {
    firstName,
    lastName,
    profesion,
    email,
    birthDate,
    ID,
    taxId,
    description,
    interests,
  } = req.body;

  const Profile: IUserProfile = new UserProfile({});

  Profile.user_profile_id = req.user.id;
  const profileFields: { [key: string]: any } = {};

  profileFields.user_profile_id = Profile.user_profile_id;
  if (firstName) {
    profileFields.firstName = firstName;
  }
  if (lastName) {
    profileFields.lastName = lastName;
  }
  if (profesion) {
    profileFields.profesion = profesion;
  }
  if (email) {
    profileFields.email = email;
  }
  if (birthDate) {
    profileFields.birthDate = birthDate;
  }
  if (ID) {
    profileFields.ID = ID;
  }
  if (taxId) {
    profileFields.taxId = taxId;
  }
  if (description) {
    profileFields.description = description;
  }
  if (interests) {
    profileFields.interests = interests;
  }

  try {
    let profile = await UserProfile.findOne({ user_profile_id: req.user.id });

    if (!profile) {
      const newProfile = new UserProfile(profileFields);
      newProfile.save();
      return res.status(200).json(newProfile);
    }

    delete profileFields._id;
    profile = await UserProfile.findByIdAndUpdate(
      { _id: profile._id },
      { $set: profileFields },
      { new: true },
    );

    return res.status(200).json({ profile });
  } catch (error) {
    // Generar errores para los campos obligatorios
    res.status(400).json(error);
  }
};

// @GET - Public Route - get all profiles

export const getAllProfiles = async (req: any, res: any) => {
  try {
    const profiles = await UserProfile.find();
    return res.status(200).json(profiles);
  } catch (error) {
    return res.status(500).json(error);
  }
};

// @GET - Public route - get a user profile by ID

export const getProfileById = async (req: any, res: any) => {
  const profileId = req.params.id;
  try {
    const userProfileById = await UserProfile.findById(profileId);
    return res.status(200).json(userProfileById);
  } catch (error) {
    return error.kind === "ObjectId"
      ? res.status(404).json({ msg: "Profile not found" })
      : res.status(500).json(error);
  }
};

// @GET - Private Route - User Deletes its profile

export const deleteProfile = async (req: any, res: any) => {
  const profileId = req.params.id;
  try {
    const userProfile = await UserProfile.findById(profileId);
    // only the user is able to delete his original profile
    if (userProfile.user_profile_id !== req.user.id) {
      return res
        .status(404)
        .json({ msg: "User not authorized to delete this profile" });
    }

    await UserProfile.findByIdAndDelete(profileId);
    return res.status(200).json({ msg: "Profile was removed" });
  } catch (error) {
    return error.kind === "ObjectId"
      ? res.status(404).json({ msg: "Profile not found" })
      : res.status(500).json(error);
  }
};

// @POST - Private Route - User adds interests

export const addInterest = async (req: any, res: any) => {
  const userId = req.params.id;
  const interest = req.body.interest;

  try {
    const userProfile = await UserProfile.findById(userId);

    userProfile.interests.unshift(interest);
    userProfile.save();

    res.status(200).json(userProfile);
  } catch (error) {
    return error.kind === "ObjectId"
      ? res.status(404).json({ msg: "Profile not found" })
      : res.status(500).json(error);
  }
};
