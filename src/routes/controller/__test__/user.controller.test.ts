import { DataConfig } from "../../../config/dataConfig";
import mongoose from "mongoose";
import request from "supertest";
import { app } from "../../../server";

beforeAll(async () => {
  await mongoose.connect(DataConfig.mongoURI, {
    useNewUrlParser: true
  });
});

afterAll(async () => {
  mongoose.connection.close();
});

test("A user logs in", async done => {
  await request(app)
    .post("/api/user/login-user")
    .send({
      email: "horus@goldendawn.com",
      password: "1234567"
    })
    .expect(200);
  done();
});

test("A user provides wrong email, and login fails", async done => {
  await request(app)
    .post("/api/user/login-user")
    .send({
      email: "horus@goldend0awn2.com",
      password: "1234567"
    })
    .expect(400);
  done();
});

test("A user provides wrong password, and login fails", async done => {
  await request(app)
    .post("/api/user/login-user")
    .send({
      email: "horus@goldendawn2.com",
      password: "12345670"
    })
    .expect(400);
  done();
});

test("A user creates a new account", async done => {
  await request(app)
    .post("/api/user/add-user")
    .send({
      name: "Fake Seth",
      email: "fakeSeth@seth.como",
      password: "12345670000"
    })
    .expect(200);
  done();
});

test("A minus 6 characters password will return error", async done => {
  await request(app)
    .post("/api/user/add-user")
    .send({
      name: "Fake Seth",
      email: "fakeSeth@seth.com",
      password: "1234"
    })
    .expect(400);
  done();
});
