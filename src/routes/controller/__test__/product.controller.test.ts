import { DataConfig } from "../../../config/dataConfig";
import mongoose from "mongoose";
import request from "supertest";
import { app } from "../../../server";

beforeAll(async () => {
  await mongoose.connect(DataConfig.mongoURI, {
    useNewUrlParser: true
  });
});

afterAll(async () => {
  mongoose.connection.close();
});

test("Service can get all the products", async done => {
  await request(app)
    .get("/api/product/all")
    .expect(200);
  done();
});

test("A new product is added to catalogue", async done => {
  await request(app)
    .post("/api/product/add-product")
    .send({
      ref: 158,
      product_name: "Book of Horus",
      product_type: "The story of the falcon god",
      price: 700,
      description: "The story of the glorious"
    })
    .expect(200);
});
