import React, { Fragment } from "react";
import { FaHome } from "react-icons/fa";
import { FaStore } from "react-icons/fa";
import { Link } from "react-router-dom";

import styles from "./layout.module.scss";

const routes = [
  { route: "/", icon: <FaHome /> },
  { route: "/book-list", icon: <FaStore /> },
];

const NavbarTop = () => {
  return (
    <Fragment>
      <div className={styles.navBar}>
        <ul>
          {routes.map((route) => (
            <li key={"xxxx" + route.route + 33}>
              <Link to={route.route}>{route.icon}</Link>
            </li>
          ))}
          <li className={styles.mainTitle}>
            <h1>Equinox Bookstore</h1>
          </li>
        </ul>
      </div>
    </Fragment>
  );
};

export default NavbarTop;
