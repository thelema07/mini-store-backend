import React, { Fragment } from "react";
import img from "../../mock/land_1.jpg";

// import styles from "./layout.module.scss";

const options = {
  title: "Equinox Bookstore",
  subtitle: "Books for real magicians",
  notes:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  link: "https://www.google.com",
  image_url: img,
};

interface NavbarProps {
  styles: any;
}

const LandingCards = ({ styles }: NavbarProps) => {
  return (
    <Fragment>
      <div className={styles.landingCard}>
        {/* <img className={styles.landCardImg} src={img} alt="img" /> */}
        <div className={styles.landCardText}>
          <h1>{options.title}</h1>
          <h3>{options.subtitle}</h3>
          <p>{options.notes}</p>
          <a href={options.link}>Go check</a>
        </div>
      </div>
    </Fragment>
  );
};

export default LandingCards;
