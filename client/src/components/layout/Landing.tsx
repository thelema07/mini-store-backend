import React, { Fragment } from "react";

import LandingCards from "./LandingCards";

import styles from "./layout.module.scss";

const Landing = () => {
  return (
    <Fragment>
      <div className={styles.landing}>
        {/* todo: this four guys will route */}

        <section className={styles.landingBlock}>
          <LandingCards styles={styles} />
        </section>

        <section className={styles.landingBlock}>
          <LandingCards styles={styles} />
        </section>

        <section className={styles.landingBlock}>
          <LandingCards styles={styles} />
        </section>

        <section className={styles.landingBlock}>
          <LandingCards styles={styles} />
        </section>
      </div>
    </Fragment>
  );
};

export default Landing;
