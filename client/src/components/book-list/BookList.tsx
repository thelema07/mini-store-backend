import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../management/actions/book-action";

const BookList = () => {
  const dispatch = useDispatch();
  const books = useSelector((state: any) => state.books.bookList);
  useEffect(() => {
    dispatch(actions.getBookList());
  }, [dispatch]);

  return (
    <div>
      {books.map((book: any) => (
        <li key={book._id}>
          {book._id} <button>Click!!!</button>
        </li>
      ))}
      <h1>Hello List!!!</h1>
    </div>
  );
};

export default BookList;
