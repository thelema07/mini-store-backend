import { createBrowserHistory } from "history";
import React, { Fragment } from "react";
import { Provider } from "react-redux";
import { Route, Router, Switch } from "react-router-dom";
import store from "./management/redux-store";

import BookList from "./components/book-list/BookList";
import Landing from "./components/layout/Landing";
import NavbarTop from "./components/layout/NavbarTop";

// Todo Internationalization i18n

const history = createBrowserHistory();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Fragment>
        <Router history={history}>
          <NavbarTop />
          <Route exact path="/" component={Landing}></Route>
          <Switch>
            <Route exact path="/book-list" component={BookList}></Route>
          </Switch>
        </Router>
      </Fragment>
    </Provider>
  );
};

export default App;
