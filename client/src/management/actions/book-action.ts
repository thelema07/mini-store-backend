import axios from "axios";
import { GET_BOOKS } from "./types";

export const getBookList = () => async (dispatch: any) => {
  try {
    const res = await axios.get("http://localhost:4500/api/product/all");
    console.log(res.data);
    dispatch({
      type: GET_BOOKS,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};
