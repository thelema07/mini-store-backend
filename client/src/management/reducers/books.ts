import { GET_BOOKS } from "../actions/types";

const initialState = {
  loading: false,
  bookList: [],
};

export default function(state = initialState, action: any) {
  switch (action.type) {
    case GET_BOOKS:
      return {
        ...state,
        loading: false,
        bookList: action.payload,
      };
    default:
      return state;
  }
}
